<?php

/**
 * @file - ExampleVariablePassInJSController for passing varible in js file
 */
namespace Drupal\example_variable_pass_in_js;

class ExampleVariablePassInJSController {

  /**
   * @return array
   */
  public function view(){

    $form['#attached']['library'][] = 'example_variable_pass_in_js/example_variable_pass_in_js';

    $build = [
      '#attached' => [
        'drupalSettings' => [
          'testvar' => 'This is first variable to pass from controller function.',
          ]
        ]
    ];

    return $build;
  }

}